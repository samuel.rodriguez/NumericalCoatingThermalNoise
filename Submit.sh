#!/bin/bash -
#PBS -l nodes=1:ppn=12
#PBS -l walltime=24:0:00

#PBS -N TNTest
#PBS -o TNTest.stdout
#PBS -e TnTest.stderr
#PBS -d .
#PBS -W umask=022
#PBS -S /bin/bash
# This is for submitting a batch job on orca.
umask 0022
module purge
export MODULEPATH=/share/apps/Modules/modulefiles:$MODULEPATH
set -x
export PATH=$(pwd -P)/bin:$PATH



mkdir Run_test
cd Run_test
cp ../QuasistaticBrownianThermalNoise .
cp ../QuasistaticBrownianThermalNoise.cpp .
cp ~/Shared_Storage/bin/LoadDealiiModules_v2.bash .

. LoadDealiiModules_v2.bash

module list

mpirun -np 12 QuasistaticBrownianThermalNoise -ksp_monitor -snes_monitor -options_table &>TNTest.out




